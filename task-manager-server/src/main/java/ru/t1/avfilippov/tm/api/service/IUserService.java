package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOne(@Nullable UserDTO model);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @Nullable
    @SneakyThrows
    UserDTO findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<UserDTO> findAll();

    @NotNull
    @SneakyThrows
    Collection<UserDTO> set(@NotNull Collection<UserDTO> models);

}
