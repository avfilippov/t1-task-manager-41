package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @Nullable
    @SneakyThrows
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    ProjectDTO remove(@NotNull String userId, @Nullable ProjectDTO model);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    @SneakyThrows
    ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    ProjectDTO removeByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @NotNull String name, String description);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
