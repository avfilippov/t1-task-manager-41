package ru.t1.avfilippov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
