package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectShowByIndexRequest;
import ru.t1.avfilippov.tm.dto.response.ProjectShowByIndexResponse;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "show project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final ProjectShowByIndexResponse response = getProjectEndpoint().showProjectByIndex(request);
        if (response.getProject() == null) throw new ProjectNotFoundException();
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

}
